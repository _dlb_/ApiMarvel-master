# Automação ApiMarvel

Descrição:
Este projeto de automação visa fazer o teste positivo da API Marvel nos métodos :

/v1/public/characters;
/v1/public/comics;
/v1/public/creators;
/v1/public/events;
/v1/public/series;
/v1/public/stories.

Configuração.
Pré condições:

Java 11;
Junit 5;
Gradle.

Faça um clone do projeto

Clone with SSH:   git@github.com:Leancb/ApiMarvel-master.git

Clone with HTTPS: https://github.com/Leancb/ApiMarvel-master.git
Execução
Após a intalação e configuração do projeot, executar a classe TesteService.java

Relatório Gradle disponível na pasta \AutomacoApiMarvel-master\build\reports\tests\test



